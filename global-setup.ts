// global-setup.ts
import { chromium, FullConfig } from '@playwright/test';
import Auth from './env.json'

/** Define type for authentications alias  */
export type Authentication = {
    password: string,
    username?: string,
    phone?: string,
    baseUrl?: string,
}

async function globalSetup(config: FullConfig) {
    const auth: Authentication = Auth;

    /** get use clause from define in config files, we have declare two object baseUrl and storageState */
    const { baseURL, storageState } = config.projects[0].use;
    /** Launching first program */
    const browser = await chromium.launch();
    const page = await browser.newPage();

    /** Go to the first / url and will redirect to login pages */
    await page.goto(baseURL!);

    await page.locator('input[name="phone"]').fill(auth.phone ?? auth.username ?? '');

    await page.locator('input[name="password"]').fill(auth.password);

    // pressing enter after type password
    await page.locator('input[name="password"]').press('Enter');

    /**
    * saving all state both cookie and localStorage to state we use state.json 
    * storageState should be found in use object in config files
    */
    await page.context().storageState({ path: storageState as string });

    await browser.close();
}

export default globalSetup;