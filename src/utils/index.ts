

export function random(min: number, max: number) {
    return Math.floor(Math.random() * max) + min
}

export function randomSliceBy(array: any[], slice_count: number) {
    let results: any[] = [];
    for (let index = 0; index < slice_count; index++) {
        results.push(array.splice(slice_count * Math.random() | 0, 1)[0]);
    }
    return results.filter(el => el !== undefined);
}