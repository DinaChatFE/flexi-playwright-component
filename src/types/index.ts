

export type RandomOptionsTypo = {
   allow?: boolean | true,
   multiple?: boolean | false
   count?: number
}

export type Select2Type = {
   select2_id?: number,
   text?: string,
   nth?: number,
   random?: RandomOptionsTypo | boolean,
   repeatable?: boolean
}

export type PageOpenOption = {
   callBackAfter?: undefined | CallableFunction,
   random_count?: number,
}
