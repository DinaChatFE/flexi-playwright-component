import { expect, Page, test } from "@playwright/test"
import { PageOpenOption } from "../types";
import { randomSliceBy } from "../utils";
import { Pageable } from "./extendable/resolve-page";


/** content class call right there */
export class DatableScrape extends Pageable {

   private mainSelector: string = '#crudTable_wrapper tr td:last-child a[data-button-type=show]';
   private paginateSelector: string = '.dataTables_paginate .paginate_button'
   private responseId: Array<number | string> = [];
   private processingSelector = "#crudTable_processing"
   private replaceRouteSyntax = "?";
   private defaultPushPageCount: number | string = 10;
   private defaultScrapePageCount: number | string = 10
   private scrapeOptions: any;

   /**
   *  @param page
   * Provide page playwright frame
   * getting resolve using facade, allow calling static
   */
   public static resolve(page: Page) {
      return new this(page);
   }
   /**
   * Start getting by selector and loop all paginate buttons, and starting getting id page by page
   *
   * We can set table element we want to scape by calling function above the scrape function
   * As default it scape from first td, selector: #crudTable_wrapper tr td:first-child
   * ```
   *  DataTable.resolve(page).scrape() : Promise<Array<number|string>>
   *
   * ```
   * in case changing cell of taken data we can simply set builder methods by providing new selectors
   * ```
   *
   *  DataTable.resolve(page).setMainSelector(selector: string).scrape() : Promise<Array<number|string>>
   *
   * ```
   * @returns
   */
   public async scrape(url?: string, options?: {
      scrape_by: 'button' | 'id'
   }): Promise<Array<number | string>> {
      this.scrapeOptions = options
      if (url) {
         await this.getPage().goto(url)
      }
      await this.waitDatatableLoaded()
      // first running when going to the pages
      this.responseId = [...this.responseId, ...(await this.evaluatePage())]

      // const lastPaginate = await this.getPage().locator(this.paginateSelector).last().textContent();

      const paginateButton = await this.getButtonPaginateMember()
      // get text of last button, make sure paginate button has number 0-9
      const lastPaginate = await paginateButton.last().textContent()
      // parse to integer

      const lengthPaginateButton = this.getDefaultScrapePageCount(parseInt((await lastPaginate) ?? '0'));
      // loop each page
      for (let index = 0; index < lengthPaginateButton; index++) {
         await paginateButton.locator(`text="${index + 1}"`).click()
         await this.waitDatatableLoaded()
         /* start scrapping getting from property @mainSelector */
         const responseIds = await this.evaluatePage();
         this.responseId = [...this.responseId, ...responseIds]
         test.setTimeout(test.info().timeout + 60 * 1000)
      }


      return this.responseId
   }
   private getDefaultScrapePageCount(getScrap: any): number {
      if (this.defaultScrapePageCount !== '*') {
         return this.defaultScrapePageCount > getScrap ? getScrap : this.defaultScrapePageCount;
      }
      return getScrap;
   }
   public async waitDatatableLoaded(): Promise<any> {
      await this.getPage().waitForSelector(this.processingSelector, { state: 'hidden' });
   }

   private async getButtonPaginateMember() {
      return await this.getPage().locator(this.paginateSelector).filter({ hasText: /([0-9]\w+)|([0-9])/g });
   }
   public getResponse() {
      return this.responseId
   }
   /** set selectors if the default id of table was change in some case @param newSelectors */
   public setMainSelector(newSelectors: string) {
      this.mainSelector = newSelectors

      return this
   }
   /**
   * Scrapping all the textContent of each cell or row
   * The Scrape depend on selector we define, @mainSelector
   * @returns Promise<Array<string|number>>
   */
   public async evaluatePage(): Promise<any> {
      if (this.scrapeOptions?.scrape_by == 'id') {
         this.mainSelector = '#crudTable_wrapper tr td:first-child'
      }
      return await this.getPage().$$eval(this.mainSelector, function (el) {
         let elm: any = [];
         el.forEach(element => {
            const content: string | undefined = element.getAttribute('href')?.replace(/\\n|\s/g, '');

            elm.push(content);
         });
         return elm
      })
   }
   public async setDefaultPushPageCount(count: number | '*') {
      this.defaultPushPageCount = count
   }

   /**
    *
    * @param options {@link PageOpenOption}
    */

   public async openPage(options?: PageOpenOption) {
      const href = await this.getPage().evaluate(() => document.location.href);
      const splitHref = href.split('?');
      let responseId = this.responseId;
      let sliceDefault: number | string = this.defaultPushPageCount;

      if (sliceDefault !== '*') {
         options = { ...options, random_count: parseInt(this.defaultPushPageCount.toString()) }
      }

      if (options?.random_count) {
         const randomOnly = options.random_count
         responseId = randomSliceBy(responseId, randomOnly)
      } else {
         if (sliceDefault !== '*') {
            responseId = responseId.slice(0, parseInt(sliceDefault.toString()))
         }
      }

      for (const response of responseId) {
         if (this.scrapeOptions?.scrape_by == undefined || this.scrapeOptions.scrape_by == 'button') {
            await this.iteratorDirect(response.toString(), options ?? {})
         } else {
            await this.iteratorOnIds(options, response, splitHref)
         }
      }
   }

   public async iteratorOnIds(options: any, resId: any, splitHref: any) {
      let routeUrl = '';
      if (options?.route_url !== undefined) {
         routeUrl = options.route_url.replace(this.replaceRouteSyntax, resId.toString());
      }
      else {
         routeUrl = splitHref[0] + '/' + resId + '/show?' + (splitHref[1] ?? '');
      }
      const route = await this.getPage().goto(routeUrl)
      if (options?.callBackAfter !== undefined) {
         options.callBackAfter(resId)
      } else {
         await expect(route?.ok()).toBeTruthy()
      }

      test.setTimeout(test.info().timeout + 60 * 1000)
   }

   public async iteratorDirect(res: string, options: PageOpenOption): Promise<void> {
      const route = await this.getPage().goto(res)
      await expect(route?.ok()).toBeTruthy()
      if (options && typeof options.callBackAfter == 'function') options.callBackAfter()

      test.setTimeout(test.info().timeout + 60 * 1000)
   }
}

