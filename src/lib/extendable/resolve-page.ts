import { Page } from "@playwright/test";


/** Extendable page using  */
export abstract class Pageable {
   private page: Page;
   /** getter page  */
   public getPage() {
      return this.page;
   }
   /**
    * initialize docs  when using associate page resolve
    * @param page
    */
   public constructor(page: Page) {
      this.page = page;
   }
}
