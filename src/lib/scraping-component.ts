import { Page } from "@playwright/test";
import { Pageable } from "./extendable/resolve-page";

export type Select2ScrapeTypo = {
   length: number,
   entries?: Array<any>
}
export class ScapeComponent extends Pageable {
   private resultOptionsSelector = ".select2-results__option"

   public static resolve(page: Page) {
      return new this(page);
   }
   /**
    * Scraping all results of options in selector .select2-results__options li
    *
    * @param entries
    * `` if entries == true ``
    *
    * return response with all result of li text content
    *
    */
   public async scapeSelect2Options(entries = false): Promise<Select2ScrapeTypo> {
      const resultsTag = await this.getPage().locator(this.resultOptionsSelector);
      const resultLength = await resultsTag.count();;

      if (entries) {
         return {
            entries: await this.getPage().$$eval(this.resultOptionsSelector, (element) => {
               let results: Array<any> = [];
               element.forEach(el => results.push(el.textContent))
               return element
            }),
            length: resultLength
         }
      }
      return { length: resultLength };
   }

   public setResultOptionsSelector(newSelector: string): this {
      this.resultOptionsSelector = newSelector
      return this
   }

   public getResultOptionsSelector(): string {
      return this.resultOptionsSelector;
   }
}
