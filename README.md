# Component scrape datatable
> Helpers work on datatable

This component is used to scrape all ids in a datatable list in [playwright](https:playwright.dev) that is based on laravel [backpack](https://backpackforlaravel.com) > 4.1. After it has collected all of the ids for each paginate, it may switch to observe all of the operations display, edit, and delete operate as expected.

### First step, install and import the library ###

To install library we have command the few
```bash 
echo @dinachatfe:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

Then install it from npm
```bash
npm i @dinachatfe/flexi-playwright-component
```
After install success, we have to import it on file we want to test, Ex: we want to test it in example.spec.ts

in file `example.spec.ts`
```javascript
    import { DatableScrape } from '@dinachatfe/flexi-playwright-component'
```
Then define first instance of class
```javascript
 const scrapper = DatableScrape.resolve(page)
```
we just define a variable scrapper and need a static function to get page, 
> note: page is instance of Page playwright, it get default when we call it in test function
### Scrape journey
Then we can scrap it now,
```javascript
const response = await  scrapper.scrap(url)
```
after call scrap method, we will get response, response variable has type array

scrap method require one argument 
-  `url` -  url of page
-  `options` - default `options = { scrape_by: button }`  
> Note: page must contains list view and datatable


There are two method to find selector and push page

1. the default `scrape_by({ scrape_by: 'button' })`-  select show button element `#crudTable_wrapper tr td:last-child a[data-button-type=show]`

2. with argument `scrape_by({scrape_by: 'id'})` it will use element select `#crudTable_wrapper tr td:first-child`
> If we Using `2` methods The first column should be the id

### Automate push page

To push preview page or other, we have to call a @openPage method

```javascript
    scrapper.openPage()
```

open page method doesn't require any parameter, but it has a few optional argument to modifies

- `route_url`, type string- we can lead this method to push our url Ex: `admin/account/?/show`. `?` is dynamic id
- `callBackAfter`, type of function- if we want to expect or set other logic of page, we can define it here. `callBackAfter` has 1 argument that is the `id`
- `random_count` type of number- it is a random number that the page must push, as default it push randomly **10**

> Options above can be use when scrape_by options equal id
.etc. `scrape(url, { scrape_by: "id" }) `

💥 Example
```javascript
    await scrapper.openPage({
        route_url: 'admin/account/?/edit',
        callBackAfter: async (id) => {
            await expect(id).not.toBeNull()
        }
    })

```


### Example: 


```javascript
    import { DatableScrape } from '@dinachatfe/flexi-playwright-component'
    //  define scrapper object class, calling static function @resolve to get page definition
    const scrapper = DatableScrape.resolve(page);

    // call scrape and pass arg 1  url, ex: admin/loan-contract
    await scrapper.scrape("admin/account?menu=default", {
        scrape_by: 'id'
    })
    
    // open all page preview by all id 
    await scrapper.openPage({
        route_url: 'admin/account/?/edit',
    })
```


# Component Add on 

If we haven't installed library yet, let's install it first
[install here](###First-step-install-and-import-the-library)

### Instance class 

To use it, we must first create an instance class, and then call the'resolve' page function as the previous component.

```javascript
    import { Component } from '@dinachatfe/flexi-playwright-component'

    const component = Component.resolve(page)
```

### Select2 Component

Select2 is a plugin select option that provides us with more features such as a nicer UI, search, and so on.
In automation, we must properly choose options before we submit testing, since the default codegen does not select the correct field and errors when we modify configuration.

#### ***1. Select option from select2***

After calling component instance class we can calling method from it
```typescript
    await component.select2('<name-of-input>', options?: {
        text?: string, 
        nth?: number,
        random?: {
            multiple?: boolean,
            count?: number
        }
    })
```

In options argument
1. `text` - full text of options
2. `nth` - option order
4. `repeatable` using select2 random choosing options in repeatable
3. `random` - random select of each options 

    we can add extra option to random by provide

    - `multiple` perform multiple options, can be use when using multiple select2

    - `count` how many option we need to select2, Note: it will work when multiple is true


#### Working Select option in repeatable

To use it just add `{repeatable: true}` to locator option

" What is repeatable? 
if we saw a card following by multiple inputs in it, that's repeatable. Repeatable has features to create delete and even reorder the form

![repeatable-img](./assets//repeatable-img.png)



## Migrate change

### Patch dist-tag: v.0.20.4

- Fixed cannot get multiple digit  on paginate button, fixed get last button paginate

- Add default maximum scraping by paginate 

- Add random open page after scrape
### Patch dis-tag: v.0.21.2

- Fixed component random remove dash sign since it was empty
