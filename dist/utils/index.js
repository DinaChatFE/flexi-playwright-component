"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.randomSliceBy = exports.random = void 0;
function random(min, max) {
    return Math.floor(Math.random() * max) + min;
}
exports.random = random;
function randomSliceBy(array, slice_count) {
    let results = [];
    for (let index = 0; index < slice_count; index++) {
        results.push(array.splice(slice_count * Math.random() | 0, 1)[0]);
    }
    return results.filter(el => el !== undefined);
}
exports.randomSliceBy = randomSliceBy;
