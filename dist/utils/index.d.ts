export declare function random(min: number, max: number): number;
export declare function randomSliceBy(array: any[], slice_count: number): any[];
