import { Page } from "@playwright/test";
import { Select2Type } from "../types";
import { Pageable } from "./extendable/resolve-page";
export declare class Component extends Pageable {
    /**
     * resolve and initialize component
     * Component required page to get locator and etc..
     * @param page
     */
    static resolve(page: Page): Component;
    /**
     * locate to select2
     *
     * @param name - provide as a input name or array name Ex: `properties` , `properties[]
     * @param locator - options as type of locators is the option we can pass as string, null or {@link Select2Type} Default if we don't provide second argument it take the first option
     *
     * @example
     * ```
     *    await Component.resolve(page).select2('properties[]', {  random: true })
     * ```
     * Or option in random
     * ```
     *    await Component.resolve(page).select2('properties[]', {  random: { multiple: true, count: 3 } })
     * ```
     *
     *
     */
    select2(name: string, locator?: Select2Type | string | null): Promise<this>;
    /**
     * choose options after locate select2
     * Default option will be take the first one
     *
     * passing any locator in side dropdown to choose specific
     *
     * we can call choose options after we call @select2 method
     * Ex:
     * ```
     *   const component = await Component.resolve(page)
     *   await component.select2(args)
     *
     *   await component.chooseOptions(...args)
     * ```
     *
     */
    chooseOptions(locator?: Select2Type | string | null): Promise<void>;
}
export default Component;
