"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatableScrape = void 0;
const resolve_page_1 = require("./extendable/resolve-page");
/** content class call right there */
class DatableScrape extends resolve_page_1.Pageable {
    mainSelector = '#crudTable_wrapper tr td:first-child';
    paginateSelector = '.dataTables_paginate .paginate_button';
    responseId = [];
    processingSelector = "#crudTable_processing";
    /**
    *  @param page
    * Provide page playwright frame
    * getting resolve using facade, allow calling static
    */
    static resolve(page) {
        return new this(page);
    }
    /**
     * Start getting by selector and loop all paginate buttons, and starting getting id page by page
     *
     * We can set table element we want to scape by calling function above the scrape function
     * As default it scape from first td, selector: #crudTable_wrapper tr td:first-child
     * ```
     *    DataTable.resolve(page).scrape() : Promise<Array<number|string>>
     *
     * ```
     * in case changing cell of taken data we can simply set builder methods by providing new selectors
     * ```
     *    DataTable.resolve(page).setMainSelector(selector: string).scrape() : Promise<Array<number|string>>
     *
     * ```
     * @returns
     */
    async scrape() {
        await this.waitDatatableLoaded();
        // first running when going to the pages
        this.responseId = [...this.responseId, ...(await this.evaluatePage())];
        const paginateButton = await this.getButtonPaginateMember();
        const lengthPaginateButton = await paginateButton.count();
        for (let index = 0; index < lengthPaginateButton - 1; index++) {
            await paginateButton.nth(index + 1).click();
            await this.waitDatatableLoaded();
            const responseIds = await this.evaluatePage();
            this.responseId = [...this.responseId, ...responseIds];
        }
        return this.responseId;
    }
    async waitDatatableLoaded() {
        await this.getPage().waitForSelector(this.processingSelector, { state: 'hidden' });
    }
    async getButtonPaginateMember() {
        return await this.getPage().locator(this.paginateSelector).filter({ hasText: /[0-9]/g });
    }
    getResponse() {
        return this.responseId;
    }
    setMainSelector(newSelectors) {
        this.mainSelector = newSelectors;
        return this;
    }
    async evaluatePage() {
        return await this.getPage().$$eval(this.mainSelector, function (el) {
            let elm = [];
            el.forEach(element => {
                const content = element.textContent?.replace(/\\n|\s/g, '');
                elm.push(content);
            });
            return elm;
        });
    }
}
exports.DatableScrape = DatableScrape;
