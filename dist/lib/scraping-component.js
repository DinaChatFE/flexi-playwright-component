"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScapeComponent = void 0;
const resolve_page_1 = require("./extendable/resolve-page");
class ScapeComponent extends resolve_page_1.Pageable {
    resultOptionsSelector = ".select2-results__option";
    static resolve(page) {
        return new this(page);
    }
    /**
     * Scraping all results of options in selector .select2-results__options li
     *
     * @param entries
     * `` if entries == true ``
     *
     * return response with all result of li text content
     *
     */
    async scapeSelect2Options(entries = false) {
        const resultsTag = await this.getPage().locator(this.resultOptionsSelector);
        const resultLength = await resultsTag.count();
        ;
        if (entries) {
            return {
                entries: await this.getPage().$$eval(this.resultOptionsSelector, (element) => {
                    let results = [];
                    element.forEach(el => results.push(el.textContent));
                    return element;
                }),
                length: resultLength
            };
        }
        return { length: resultLength };
    }
    setResultOptionsSelector(newSelector) {
        this.resultOptionsSelector = newSelector;
        return this;
    }
    getResultOptionsSelector() {
        return this.resultOptionsSelector;
    }
}
exports.ScapeComponent = ScapeComponent;
