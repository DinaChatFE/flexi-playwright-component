import { Page } from "@playwright/test";
import { Pageable } from "./extendable/resolve-page";
export declare type Select2ScrapeTypo = {
    length: number;
    entries?: Array<any>;
};
export declare class ScapeComponent extends Pageable {
    private resultOptionsSelector;
    static resolve(page: Page): ScapeComponent;
    /**
     * Scraping all results of options in selector .select2-results__options li
     *
     * @param entries
     * `` if entries == true ``
     *
     * return response with all result of li text content
     *
     */
    scapeSelect2Options(entries?: boolean): Promise<Select2ScrapeTypo>;
    setResultOptionsSelector(newSelector: string): this;
    getResultOptionsSelector(): string;
}
