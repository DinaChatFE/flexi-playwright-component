import { Page } from "@playwright/test";
import { Pageable } from "./extendable/resolve-page";
/** content class call right there */
export declare class DatableScrape extends Pageable {
    private mainSelector;
    private paginateSelector;
    private responseId;
    private processingSelector;
    /**
    *  @param page
    * Provide page playwright frame
    * getting resolve using facade, allow calling static
    */
    static resolve(page: Page): DatableScrape;
    /**
     * Start getting by selector and loop all paginate buttons, and starting getting id page by page
     *
     * We can set table element we want to scape by calling function above the scrape function
     * As default it scape from first td, selector: #crudTable_wrapper tr td:first-child
     * ```
     *    DataTable.resolve(page).scrape() : Promise<Array<number|string>>
     *
     * ```
     * in case changing cell of taken data we can simply set builder methods by providing new selectors
     * ```
     *    DataTable.resolve(page).setMainSelector(selector: string).scrape() : Promise<Array<number|string>>
     *
     * ```
     * @returns
     */
    scrape(): Promise<Array<number | string>>;
    waitDatatableLoaded(): Promise<any>;
    private getButtonPaginateMember;
    getResponse(): (string | number)[];
    setMainSelector(newSelectors: string): this;
    evaluatePage(): Promise<any>;
}
