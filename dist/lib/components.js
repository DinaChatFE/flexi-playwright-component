"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Component = void 0;
const utils_1 = require("../utils");
const resolve_page_1 = require("./extendable/resolve-page");
class Component extends resolve_page_1.Pageable {
    /**
     * resolve and initialize component
     * Component required page to get locator and etc..
     * @param page
     */
    static resolve(page) {
        return new this(page);
    }
    /**
     * locate to select2
     *
     * @param name - provide as a input name or array name Ex: `properties` , `properties[]
     * @param locator - options as type of locators is the option we can pass as string, null or {@link Select2Type} Default if we don't provide second argument it take the first option
     *
     * @example
     * ```
     *    await Component.resolve(page).select2('properties[]', {  random: true })
     * ```
     * Or option in random
     * ```
     *    await Component.resolve(page).select2('properties[]', {  random: { multiple: true, count: 3 } })
     * ```
     *
     *
     */
    async select2(name, locator = null) {
        let count = 1;
        if (typeof locator == 'object' && (typeof locator?.random == 'object' && locator.random.multiple === true)) {
            count = locator.random.count ?? (0, utils_1.random)(1, 2);
        }
        for (let index = 0; index < count; index++) {
            let keys = 'name';
            if (typeof locator == 'object' && locator?.repeatable === true) {
                keys = 'data-repeatable-input-name';
            }
            ;
            await this.getPage().locator(`//select[@${keys}="${name}"]/following-sibling::span`).click();
            await this.chooseOptions(locator);
        }
        return this;
    }
    /**
     * choose options after locate select2
     * Default option will be take the first one
     *
     * passing any locator in side dropdown to choose specific
     *
     * we can call choose options after we call @select2 method
     * Ex:
     * ```
     *   const component = await Component.resolve(page)
     *   await component.select2(args)
     *
     *   await component.chooseOptions(...args)
     * ```
     *
     */
    async chooseOptions(locator = null) {
        if (locator == null) {
            await this.getPage().locator('.select2-results__option').filter({ hasText: /^(?:(?!-).)*$/g }).nth(0).click();
        }
        else {
            if (typeof (locator) == 'object') {
                const normalizeLocator = this.getPage().locator('.select2-results__option').filter({ hasText: /^(?:(?!-).)*$/g });
                /** locate random by nth order select options */
                if (locator.random === true || (typeof (locator.random) === 'object' && locator.random.allow !== false)) {
                    const scrapCom = this.getPage().locator('.select2-results__option').filter({ hasText: /Searching/g });
                    if (await scrapCom.count()) {
                        await this.getPage().locator('.select2-results__option').filter({ hasText: /Searching/g }).waitFor({ 'state': 'hidden' });
                    }
                    const targetLength = await normalizeLocator.count();
                    const randomNth = (0, utils_1.random)(0, targetLength - 1);
                    await normalizeLocator.nth(randomNth).click();
                }
                else if (locator.nth) {
                    await normalizeLocator.nth(locator.nth - 1).click();
                }
                else if (locator.text) {
                    await normalizeLocator.locator('text=' + locator.text + '').click();
                }
                else if (locator.select2_id) {
                    /** find select 2 id, was existed in li class select2-result-option and has data attr select2-id */
                    await this.getPage().locator(`//*[contains(@class,  "select2-results__option") and @data-select2-id=${locator.select2_id}"]`).click();
                }
                else {
                    await this.chooseOptions(null);
                }
            }
            else {
                await this.getPage().locator(locator).click();
            }
        }
    }
}
exports.Component = Component;
exports.default = Component;
