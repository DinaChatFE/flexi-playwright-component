import { Page } from "@playwright/test";
/** Extendable page using  */
export declare abstract class Pageable {
    private page;
    /** getter page  */
    getPage(): Page;
    /**
     * initialize docs  when using associate page resolve
     * @param page
     */
    constructor(page: Page);
}
