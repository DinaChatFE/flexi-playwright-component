"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pageable = void 0;
/** Extendable page using  */
class Pageable {
    page;
    /** getter page  */
    getPage() {
        return this.page;
    }
    /**
     * initialize docs  when using associate page resolve
     * @param page
     */
    constructor(page) {
        this.page = page;
    }
}
exports.Pageable = Pageable;
