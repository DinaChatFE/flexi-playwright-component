"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatableScrape = void 0;
const test_1 = require("@playwright/test");
const utils_1 = require("../utils");
const resolve_page_1 = require("./extendable/resolve-page");
/** content class call right there */
class DatableScrape extends resolve_page_1.Pageable {
    mainSelector = '#crudTable_wrapper tr td:last-child a[data-button-type=show]';
    paginateSelector = '.dataTables_paginate .paginate_button';
    responseId = [];
    processingSelector = "#crudTable_processing";
    replaceRouteSyntax = "?";
    defaultPushPageCount = 10;
    defaultScrapePageCount = 10;
    /**
    *  @param page
    * Provide page playwright frame
    * getting resolve using facade, allow calling static
    */
    static resolve(page) {
        return new this(page);
    }
    /**
    * Start getting by selector and loop all paginate buttons, and starting getting id page by page
    *
    * We can set table element we want to scape by calling function above the scrape function
    * As default it scape from first td, selector: #crudTable_wrapper tr td:first-child
    * ```
    *  DataTable.resolve(page).scrape() : Promise<Array<number|string>>
    *
    * ```
    * in case changing cell of taken data we can simply set builder methods by providing new selectors
    * ```
    *
    *  DataTable.resolve(page).setMainSelector(selector: string).scrape() : Promise<Array<number|string>>
    *
    * ```
    * @returns
    */
    async scrape(url) {
        if (url) {
            await this.getPage().goto(url);
        }
        await this.waitDatatableLoaded();
        // first running when going to the pages
        this.responseId = [...this.responseId, ...(await this.evaluatePage())];
        // const lastPaginate = await this.getPage().locator(this.paginateSelector).last().textContent();
        const paginateButton = await this.getButtonPaginateMember();
        // get text of last button, make sure paginate button has number 0-9
        const lastPaginate = await paginateButton.last().textContent();
        // parse to integer
        const lengthPaginateButton = this.getDefaultScrapePageCount(parseInt((await lastPaginate) ?? '0'));
        // loop each page
        for (let index = 0; index < lengthPaginateButton; index++) {
            await paginateButton.locator(`text="${index + 1}"`).click();
            await this.waitDatatableLoaded();
            /* start scrapping getting from property @mainSelector */
            const responseIds = await this.evaluatePage();
            this.responseId = [...this.responseId, ...responseIds];
            test_1.test.setTimeout(test_1.test.info().timeout + 60 * 1000);
        }
        return this.responseId;
    }
    getDefaultScrapePageCount(getScrap) {
        if (this.defaultScrapePageCount !== '*') {
            return this.defaultScrapePageCount > getScrap ? getScrap : this.defaultScrapePageCount;
        }
        return getScrap;
    }
    async waitDatatableLoaded() {
        await this.getPage().waitForSelector(this.processingSelector, { state: 'hidden' });
    }
    async getButtonPaginateMember() {
        return await this.getPage().locator(this.paginateSelector).filter({ hasText: /([0-9]\w+)|([0-9])/g });
    }
    getResponse() {
        return this.responseId;
    }
    /** set selectors if the default id of table was change in some case @param newSelectors */
    setMainSelector(newSelectors) {
        this.mainSelector = newSelectors;
        return this;
    }
    /**
    * Scrapping all the textContent of each cell or row
    * The Scrape depend on selector we define, @mainSelector
    * @returns Promise<Array<string|number>>
    */
    async evaluatePage() {
        return await this.getPage().$$eval(this.mainSelector, function (el) {
            let elm = [];
            el.forEach(element => {
                const content = element.getAttribute('href')?.replace(/\\n|\s/g, '');
                elm.push(content);
            });
            return elm;
        });
    }
    async setDefaultPushPageCount(count) {
        this.defaultPushPageCount = count;
    }
    async openPage(options) {
        const href = await this.getPage().evaluate(() => document.location.href);
        const splitHref = href.split('?');
        let responseId = this.responseId;
        let sliceDefault = this.defaultPushPageCount;
        if (sliceDefault !== '*') {
            options = { ...options, random_count: parseInt(this.defaultPushPageCount.toString()) };
        }
        if (options?.random_count) {
            const randomOnly = options.random_count;
            responseId = (0, utils_1.randomSliceBy)(responseId, randomOnly);
        }
        else {
            if (sliceDefault !== '*') {
                responseId = responseId.slice(0, parseInt(sliceDefault.toString()));
            }
        }
        for (const response of responseId) {
            if (options?.scrap_by == undefined || options?.scrap_by == 'url') {
                await this.iteratorDirect(response.toString(), options ?? {});
            }
            else {
                await this.iteratorOnIds(options, response, splitHref);
            }
        }
    }
    async iteratorOnIds(options, resId, splitHref) {
        let routeUrl = '';
        if (options?.route_url !== undefined) {
            routeUrl = options.route_url.replace(this.replaceRouteSyntax, resId.toString());
        }
        else {
            routeUrl = splitHref[0] + '/' + resId + '/show?' + (splitHref[1] ?? '');
        }
        const route = await this.getPage().goto(routeUrl);
        if (options?.callBackAfter !== undefined) {
            options.callBackAfter(resId);
        }
        else {
            await (0, test_1.expect)(route?.ok()).toBeTruthy();
        }
        test_1.test.setTimeout(test_1.test.info().timeout + 60 * 1000);
    }
    async iteratorDirect(res, options) {
        const route = await this.getPage().goto(res);
        await (0, test_1.expect)(route?.ok()).toBeTruthy();
        if (options && typeof options.callBackAfter == 'function')
            options.callBackAfter();
        test_1.test.setTimeout(test_1.test.info().timeout + 60 * 1000);
    }
}
exports.DatableScrape = DatableScrape;
