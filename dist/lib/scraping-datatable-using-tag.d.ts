import { Page } from "@playwright/test";
import { PageOpenOption } from "../types";
import { Pageable } from "./extendable/resolve-page";
/** content class call right there */
export declare class DatableScrape extends Pageable {
    private mainSelector;
    private paginateSelector;
    private responseId;
    private processingSelector;
    private replaceRouteSyntax;
    private defaultPushPageCount;
    private defaultScrapePageCount;
    /**
    *  @param page
    * Provide page playwright frame
    * getting resolve using facade, allow calling static
    */
    static resolve(page: Page): DatableScrape;
    /**
    * Start getting by selector and loop all paginate buttons, and starting getting id page by page
    *
    * We can set table element we want to scape by calling function above the scrape function
    * As default it scape from first td, selector: #crudTable_wrapper tr td:first-child
    * ```
    *  DataTable.resolve(page).scrape() : Promise<Array<number|string>>
    *
    * ```
    * in case changing cell of taken data we can simply set builder methods by providing new selectors
    * ```
    *
    *  DataTable.resolve(page).setMainSelector(selector: string).scrape() : Promise<Array<number|string>>
    *
    * ```
    * @returns
    */
    scrape(url?: string): Promise<Array<number | string>>;
    private getDefaultScrapePageCount;
    waitDatatableLoaded(): Promise<any>;
    private getButtonPaginateMember;
    getResponse(): (string | number)[];
    /** set selectors if the default id of table was change in some case @param newSelectors */
    setMainSelector(newSelectors: string): this;
    /**
    * Scrapping all the textContent of each cell or row
    * The Scrape depend on selector we define, @mainSelector
    * @returns Promise<Array<string|number>>
    */
    evaluatePage(): Promise<any>;
    setDefaultPushPageCount(count: number | '*'): Promise<void>;
    openPage(options?: PageOpenOption): Promise<void>;
    iteratorOnIds(options: any, resId: any, splitHref: any): Promise<void>;
    iteratorDirect(res: string, options: PageOpenOption): Promise<void>;
}
